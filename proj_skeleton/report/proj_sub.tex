\documentclass[12pt]{article}

\usepackage[letterpaper, hmargin=0.5in, vmargin=0.5in]{geometry}
\usepackage{float}
\usepackage{url}
\usepackage{listings}
\usepackage{xcolor}
\usepackage{graphicx}

%New colors defined below
\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

%Code listing style named "mystyle"
\lstdefinestyle{mystyle}{
  backgroundcolor=\color{backcolour},   commentstyle=\color{codegreen},
  keywordstyle=\color{magenta},
  numberstyle=\tiny\color{codegray},
  stringstyle=\color{codepurple},
  basicstyle=\ttfamily\footnotesize,
  breakatwhitespace=false,         
  breaklines=true,                 
  captionpos=b,                    
  keepspaces=true,                 
  numbers=left,                    
  numbersep=5pt,                  
  showspaces=false,                
  showstringspaces=false,
  showtabs=false,                  
  tabsize=2
}
\lstset{style=mystyle}

\pagestyle{empty}

\title{SE465 -- Course Project}
\author{
  Saidi Tang\\
  20703926\\
  \texttt{s69tang@edu.uwaterloo.ca}
  \and
  Haolei Wang\\
  20711840\\
  \texttt{h585wang@edu.uwaterloo.ca}
  \and
  Hongru Xiang\\
  20734454\\
  \texttt{h4xiang@edu.uwaterloo.ca}
}
\date{April 6, 2020}

\begin{document}

\maketitle

\section*{Question b)}
There are two fundamental reasons for false positives to occur in general:
\begin{itemize}
    \item One reason is that there are many occasions that we will always pass in same arguments to a function, or we will need to pre-process the arguments in the same way and then pass them to a function that we do not have the authority to modify (it can a operating system function that we cannot easily change). As a result, developers will create a wrapper function on top of the original function. Then, if there is a wrapper function $A'$ created for function $A$ of pair $(A,B)$, then it is expected to only call $A$ inside the wrapper function $A'$ but not function $B$. However, if pair $(A,B)$ has high support and confidence, our analysis will report this as a bug since we are only calling $A$ but not $B$ inside the wrapper function $A'$.
    \item Another reason is that there could be two closely coupled logic abstractions, and one abstraction has more than one implementations, and the other has only one implementation. Moreover, almost all times, one of the former abstraction is used. Examples of that is memory allocation and object creation (call it function $C$). There are two ways to allocate memory, allocate on heap (call it function $A$) and allocate on stack (call it function $B$). Some objects can be mostly allocated on heap, so pair $(A,C)$ will have high confidence than $(B,C)$. Furthermore, since $A$ and $B$ are two implementations, so they are naturally disjoint and should not be called together. Therefore, our analysis will report bugs for all occurrence of $(B,C)$ since $A$ is not called in those scopes. However, this should not be a bug, and it is expected to behave like this.
\end{itemize}
We have also identified 2 pairs of functions that are false positives in {\tt httpd}:
\begin{itemize}
    \item {\tt pair: (apr\_array\_make, apr\_array\_push)} which has appeared 16 times with support level being 10 and confidence level being 80\%. According to the documentation in the source code, {\tt apr\_array\_make} should create an array from a memory pool, and {\tt apr\_array\_push} should add a new element to an array.
    
    \begin{lstlisting}[language=java, caption=apr\_array\_make definition]
/**
 * Determine if the array is empty (either NULL or having no elements).
 * @param a The array to check
 * @return True if empty, False otherwise
 */
APR_DECLARE(int) apr_is_empty_array(const apr_array_header_t *a);
    \end{lstlisting}
    
    \begin{lstlisting}[language=java, caption=apr\_array\_push definition]
/**
 * Add a new element to an array (as a first-in, last-out stack).
 * @param arr The array to add an element to.
 * @return Location for the new element in the array.
 * @remark If there are no free spots in the array, then this function will
 *         allocate new space for the new element.
 */
APR_DECLARE(void *) apr_array_push(apr_array_header_t *arr);
    \end{lstlisting}
    
    Therefore, it makes sense for these two functions to appear in the same scope since most of the time one would create an array and insert some elements to it. However, in some cases, we may not have enough information to insert element to the array in one scope, and we can only know the information in later calls to different functions. For example, bug reported for {\tt create\_core\_dir\_config} and {\tt create\_core\_server\_config} just create per-directory and per-server configuration structure, so not calling {\tt apr\_array\_push} is not a bug since we do not have enough information on the configurations to be added in those scopes.\\
    Similarly, we will also have times when we have already created an array and will only need to insert elements to the array, but we will not need to create a new array and copy all the inserted elements over to the new one. Functions like {\tt ap\_add\_per\_dir\_conf}, {\tt ap\_add\_per\_url\_conf}, and {\tt ap\_add\_file\_conf} are examples.
    
    
    \item {\tt pair: (apr\_thread\_mutex\_lock, apr\_thread\_mutex\_unlock)} which has appeared 6 times with support level being 10 and confidence level being 80\%. From the source code documentation, these two methods should enable and disable the mutual exclusion on some critical sections by acquiring and releasing a lock.
    
    \begin{lstlisting}[language=java, caption=apr\_thread\_mutex\_lock definition]
/**
 * Acquire the lock for the given mutex. If the mutex is already locked,
 * the current thread will be put to sleep until the lock becomes available.
 * @param mutex the mutex on which to acquire the lock.
 */
APR_DECLARE(apr_status_t) apr_thread_mutex_lock(apr_thread_mutex_t *mutex);
    \end{lstlisting}
    
    \begin{lstlisting}[language=java, caption=apr\_thread\_mutex\_unlock definition]
/**
 * Release the lock for the given mutex.
 * @param mutex the mutex from which to release the lock.
 */
APR_DECLARE(apr_status_t) apr_thread_mutex_unlock(apr_thread_mutex_t *mutex);
    \end{lstlisting}
    At a glance, it would appear that lock and unlock function should be called in the same scope to avoid starving in other threads, but our analysis is only intra-procedural, if there is a helper function for lock/unlock, they are not accounted in our analysis. That is exactly how the 6 bugs are reported for this pair. There are actually 3 pairs of helper methods:
    \begin{itemize}
        \item {\tt apr\_dbd\_mutex\_lock} and {\tt apr\_dbd\_mutex\_unlock}.
        \item {\tt apu\_dso\_mutex\_lock} and {\tt apu\_dso\_mutex\_unlock}.
        \item {\tt apr\_global\_mutex\_trylock} and {\tt apr\_global\_mutex\_unlock}.
    \end{itemize}
    They are all helper methods to lock and unlock on a pre-set mutex pointer, and they should be just calling one of lock or unlock function.
\end{itemize}



\section*{Question c)}
\includegraphics[width=\textwidth]{line_cvg.png}

\section*{Question d)}

\section*{Question e)}

\end{document}
