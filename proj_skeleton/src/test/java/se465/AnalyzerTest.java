package se465;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class AnalyzerTest {

    @Test
    public void testAnalyzer1() {
        Analyzer analyzer = new Analyzer(1, 50);
        CallGraph cg = new CallGraph();
        CallGraphNode a = new CallGraphNode("a");
        CallGraphNode b = new CallGraphNode("b");
        CallGraphNode c = new CallGraphNode("c");
        CallGraphNode d = new CallGraphNode("d");

        cg.addEdge(a, b);
        cg.addEdge(a, c);
        cg.addEdge(a, d);
        cg.addEdge(b, d);

        analyzer.consume(cg);
        String[] report = analyzer.reportAnalytics().split(String.format("%n"), 0);
        Arrays.sort(report);
        assertEquals(String.format("bug: d in b, pair: (b, d), support: 1, confidence: 50.00%%%n" +
                        "bug: d in b, pair: (c, d), support: 1, confidence: 50.00%%%n"),
                String.join(String.format("%n"), report) + String.format("%n"));
    }

}
