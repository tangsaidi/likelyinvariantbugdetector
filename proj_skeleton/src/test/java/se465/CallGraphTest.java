package se465;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Iterator;

import static org.junit.Assert.*;


@RunWith(JUnit4.class)
public class CallGraphTest {
    @Before
    public void setup() throws IOException {
        File fobj = new File("testGraph.txt");
        if (fobj.createNewFile()) {
            System.out.println("File created: " + fobj.getName());
        } else {
            System.out.println("File already exists.");
        }
        FileOutputStream fs = new FileOutputStream("testGraph.txt");
        fs.write(String.format("CallGraph Root is: main%n" +
                "Call graph node <<null function>><<0x1c42020>>  #uses=0%n" +
                "  CS<0x0> calls function 'main'%n" +
                "  CS<0x0> calls external node%n" +
                "%n" +
                "Call graph node for function: 'main'<<0x1c39a30>>  #uses=1%n" +
                "  CS<0x0> calls function 'printf'%n" +
                "  CS<0x0> malformed entry here%n" +
                "%n" +
                "Call graph node for function: 'printf'<<0x1c39a30>>  #uses=1%n" +
                "  CS<0x0> calls external node%n").getBytes(Charset.forName("UTF-8")));

        fs.close();
    }
    @Test
    public void testConstructor()
    {
        CallGraph callGraph = new CallGraph();
        assertFalse(callGraph.iterator().hasNext());
    }

    @Test
    public void testAddNode()
    {
        CallGraph callGraph = new CallGraph();
        CallGraphNode a = new CallGraphNode("a");
        CallGraphNode b = new CallGraphNode("b");
        callGraph.addNode(a);
        callGraph.addNode(b);
        assertEquals(0, callGraph.getUsedCount(a));
        assertEquals(0, callGraph.getUsedCount(b));
    }

    @Test
    public void testAddEdge()
    {
        CallGraph callGraph = new CallGraph();
        CallGraphNode a = new CallGraphNode("a");
        CallGraphNode b = new CallGraphNode("b");
        callGraph.addEdge(a, b);
        assertEquals(0, callGraph.getUsedCount(a));
        assertEquals(1, callGraph.getUsedCount(b));
    }

    @Test
    public void testGetUsedBy()
    {
        CallGraph callGraph = new CallGraph();
        CallGraphNode a = new CallGraphNode("a");
        CallGraphNode b = new CallGraphNode("b");
        callGraph.addEdge(a, b);
        assertEquals(0, callGraph.getUsedBy(a).size());
        assertTrue(callGraph.getUsedBy(b).contains(a));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testIteratorRemove()
    {
        CallGraph callGraph = new CallGraph();
        CallGraphNode a = new CallGraphNode("a");
        callGraph.addNode(a);
        Iterator it = callGraph.iterator();
        while (it.hasNext()) {
            it.next();
            it.remove();
        }
    }


    @Test
    public void testGetAllChildrenPairs()
    {
        CallGraph callGraph = new CallGraph();
        CallGraphNode a = new CallGraphNode("a");
        CallGraphNode b = new CallGraphNode("b");
        CallGraphNode c = new CallGraphNode("c");
        CallGraphNode d = new CallGraphNode("_d");

        callGraph.addEdge(a, b);
        callGraph.addEdge(a, c);
        callGraph.addEdge(a, d);

        assertEquals(3, callGraph.getAllChildrenPairs(a).size());
        assertEquals(0, callGraph.getAllChildrenPairs(b).size());
        assertTrue(callGraph.getAllChildrenPairs(a).contains(new Pair<>(b, c)));
        assertTrue(callGraph.getAllChildrenPairs(a).contains(new Pair<>(d, b)));
        assertTrue(callGraph.getAllChildrenPairs(a).contains(new Pair<>(d, c)));
    }
    @Test
    public void testGetAllChildrenPairs2()
    {
        CallGraph callGraph = new CallGraph();
        CallGraphNode a = new CallGraphNode("a");
        CallGraphNode b = new CallGraphNode("b");
        CallGraphNode c = new CallGraphNode("c");

        callGraph.addEdge(a, c);
        callGraph.addEdge(a, b);

        assertEquals(1, callGraph.getAllChildrenPairs(a).size());
        assertTrue(callGraph.getAllChildrenPairs(a).contains(new Pair<>(b, c)));

    }
    @After
    public void tearDown()
    {
        File file = new File("testGraph.txt");

        if(file.delete())
        {
            System.out.println(file.getName() + ": File deleted successfully");
        }
        else
        {
            System.out.println(file.getName() + ": Failed to delete the file at " + file.getAbsolutePath());
        }
    }
}
