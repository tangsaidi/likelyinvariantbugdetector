package se465;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class MainTest {

  private Main o;

  private final ByteArrayOutputStream newOut = new ByteArrayOutputStream();
  private final PrintStream sysOut = System.out;

  private final String TEST_DIR_PATH = "testDir";
  private final String TEST_GRAPH_FILE_PATH = "testGraph.txt";
  private final String TEST_OUTPUT_FILE_PATH = "testOutput.txt";

  @Before
  public void setup() throws IOException {
    o = new Main();
    File dir = new File(TEST_DIR_PATH);
    if (dir.mkdir()) {
      System.out.println("Temporary directory created");
    } else {
      System.out.println("Temporary directory already exists");
    }
    File fobj = new File(TEST_GRAPH_FILE_PATH);
    if (fobj.createNewFile()) {
      System.out.println("File created: " + fobj.getName());
    } else {
      System.out.println("File already exists.");
    }
    FileOutputStream fs = new FileOutputStream(TEST_GRAPH_FILE_PATH);
    fs.write(String.format("CallGraph Root is: main%n" +
            "Call graph node <<null function>><<0x1c42020>>  #uses=0%n" +
            "  CS<0x0> calls function 'main'%n" +
            "  CS<0x0> calls external node%n" +
            "%n" +
            "Call graph node for function: 'a'<<0x1c39a30>>  #uses=1%n" +
            "  CS<0x0> calls function 'b'%n" +
            "  CS<0x0> calls function 'c'%n" +
            "  CS<0x0> calls function 'd'%n" +
            "  CS<0x0> malformed entry here%n" +
            "%n" +
            "Call graph node for function: 'b'<<0x1c39a30>>  #uses=1%n" +
            "  CS<0x0> calls function 'd'%n" +
            "  CS<0x0> calls external node%n").getBytes(Charset.forName("UTF-8")));
    fs.close();
    System.setOut(new PrintStream(newOut));
  }

  /* add your test code here */
  @Test(expected = IllegalArgumentException.class)
  public void testInvalidCLIArguments() {
    Main.main(new String[]{"invalid", "args"});
  }

  @Test(expected = IllegalArgumentException.class)
  public void testMissingRquiredCLIArguments() {
    Main.main(new String[]{"--support", "3", "--confidence", "5"});
  }

  @Test(expected = RuntimeException.class)
  public void testInvalidOutput() {
    Main.main(new String[]{"--support", "3", "--confidence", "5",
            "--callgraph", TEST_GRAPH_FILE_PATH, "--output", TEST_DIR_PATH});
  }

  @Test()
  public void testManualHelperMissingArg() {
    try {
      Main.main(new String[]{"--support", "3", "--confidence", "5",
              "--callgraph", TEST_GRAPH_FILE_PATH});
    } catch (Exception e) {
      assertEquals(String.format("Please enter arguments as follows:%n" +
              "--support <T_SUPPORT>%n" +
              "--confidence <T_CONFIDENCE>%n" +
              "--callgraph <CALLGRAPH_FILE>%n" +
              "--output <OUTPUT_FILE>%n" +
              "Note: --callgraph and --output flags are mandatory%n"), newOut.toString());
    }
  }

  @Test()
  public void testManualHelperInvalidArg() {
    try {
      Main.main(new String[]{"--invalid", "3", "--confidence", "5",
              "--callgraph", TEST_GRAPH_FILE_PATH});
    } catch (Exception e) {
      assertEquals(String.format("Please enter arguments as follows:%n" +
              "--support <T_SUPPORT>%n" +
              "--confidence <T_CONFIDENCE>%n" +
              "--callgraph <CALLGRAPH_FILE>%n" +
              "--output <OUTPUT_FILE>%n" +
              "Note: --callgraph and --output flags are mandatory%n"), newOut.toString());
    }
  }

  @Test
  public void testFlow() {
    Main.main(new String[]{"--support", "1", "--confidence", "50",
            "--callgraph", TEST_GRAPH_FILE_PATH, "--output", TEST_OUTPUT_FILE_PATH});
    File fobj = new File(TEST_OUTPUT_FILE_PATH);
    byte[] result;
    try (FileInputStream is = new FileInputStream(fobj)){
      result = new byte[(int) fobj.length()];
      is.read(result);
    } catch (IOException e) {
      System.out.println("** File Not Found Error");
      throw new RuntimeException(e.getMessage());
    }
    assertEquals(String.format("bug: d in b, pair: (b, d), support: 1, confidence: 50.00%%%n" +
            "bug: d in b, pair: (c, d), support: 1, confidence: 50.00%%%n"), new String(result, StandardCharsets.UTF_8));
  }

  @After
  public void tearDown() throws IOException {
    System.setOut(sysOut);
    newOut.close();

    File graphFile = new File(TEST_GRAPH_FILE_PATH);
    File outputFile = new File(TEST_OUTPUT_FILE_PATH);
    File dir = new File(TEST_DIR_PATH);

    deleteFileHelper(graphFile);
    deleteFileHelper(outputFile);
    deleteFileHelper(dir);
  }

  private void deleteFileHelper(File file) {
    if (file.exists()) {
      if (file.delete()) {
        System.out.println(file.getName() + ": Files deleted successfully");
      } else {
        System.out.println(file.getName() + ": Failed to delete the file at" + file.getAbsolutePath());
      }
    }
  }
}
