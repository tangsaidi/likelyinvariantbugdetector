package se465;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class CallGraphNodeTest {

    @Test
    public void testEqualToSelf()
    {
        CallGraphNode a = new CallGraphNode("a");
        assertEquals("Object not equal to itself", a, a);
    }

    @Test
    public void testNotEqualToNull()
    {
        CallGraphNode a = new CallGraphNode("a");
        assertNotEquals("Object comparison with null returned true", a, null);
    }

    @Test
    public void testNotEqualToOther()
    {
        CallGraphNode a = new CallGraphNode("a");
        assertNotEquals("Object comparison with other object returned true", a, new Object());
    }

    @Test
    public void testToString()
    {
        CallGraphNode node = new CallGraphNode("abc");
        assertEquals(node.toString(), "abc");
    }

    @Test
    public void testGetID()
    {
        CallGraphNode node = new CallGraphNode("abc");
        assertEquals(node.getId(), "abc");
    }

    @Test
    public void testHashCode()
    {
        CallGraphNode a = new CallGraphNode("abc");
        assertEquals(a.hashCode(), "abc".hashCode());
    }

    @Test
    public void testEqual()
    {
        CallGraphNode a = new CallGraphNode("abc");
        CallGraphNode another_a = new CallGraphNode("abc");
        assertEquals(a, another_a);
    }

    @Test
    public void testNotEqual()
    {
        CallGraphNode a = new CallGraphNode("abc");
        CallGraphNode b = new CallGraphNode("abd");
        assertNotEquals(a, b);
    }

    @Test
    public void testCompareTo()
    {
        CallGraphNode a = new CallGraphNode("abc");
        CallGraphNode another_a = new CallGraphNode("abc");
        CallGraphNode b = new CallGraphNode("abd");
        assertTrue(a.compareTo(b) < 0);
        assertTrue(b.compareTo(a) > 0);
        assertEquals(0, a.compareTo(another_a));
        assertEquals(0,another_a.compareTo(a) );
    }


}

