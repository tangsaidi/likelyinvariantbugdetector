package se465;

import jdk.nashorn.internal.codegen.CompilerConstants;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.*;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class GraphParserTest {
    private final ByteArrayOutputStream newOut = new ByteArrayOutputStream();
    private final PrintStream sysOut = System.out;
    @Before
    public void setup() throws IOException{
        File fobj = new File("testGraph.txt");
        if (fobj.createNewFile()) {
            System.out.println("File created: " + fobj.getName());
        } else {
            System.out.println("File already exists.");
        }
        FileOutputStream fs = new FileOutputStream("testGraph.txt");
        fs.write(String.format("CallGraph Root is: main%n" +
                "Call graph node <<null function>><<0x1c42020>>  #uses=0%n" +
                "  CS<0x0> calls function 'main'%n" +
                "  CS<0x0> calls external node%n" +
                "%n" +
                "Call graph node for function: 'main'<<0x1c39a30>>  #uses=1%n" +
                "  CS<0x0> calls function 'printf'%n" +
                "  CS<0x0> malformed entry here%n" +
                "%n" +
                "Call graph node for function: 'printf'<<0x1c39a30>>  #uses=1%n" +
                "  CS<0x0> calls external node%n").getBytes(Charset.forName("UTF-8")));
        fs.close();

        FileOutputStream emptyFs = new FileOutputStream("empty.txt");
        emptyFs.write("".getBytes(Charset.forName("UTF-8")));
        emptyFs.close();

        System.setOut(new PrintStream(newOut));
    }

    @Test
    public void testEmptyFile() {
        CallGraph cg = (new GraphParser()).parse("empty.txt");
        assertFalse(cg.iterator().hasNext());
    }

    @Test(expected = RuntimeException.class)
    public void testFalseFile()
    {
        (new GraphParser()).parse("Non-existent file");
    }
    @Test()
    public void testFalseFileOutput()
    {
        try {
            (new GraphParser()).parse("Non-existent file");
        } catch (RuntimeException e) {
            assertEquals(String.format("** File Not Found Error%n"), newOut.toString());
            return;
        }
        fail();
    }

    @Test
    public void testInputFile()
    {
        CallGraph callGraph = (new GraphParser()).parse( "testGraph.txt");
        assertNotNull(callGraph);
        Iterator<CallGraphNode> it = callGraph.iterator();
        Set<CallGraphNode> expected = new HashSet<>();
        expected.add(new CallGraphNode("main"));
        expected.add(new CallGraphNode("printf"));
        while (it.hasNext()) {
            assertTrue(expected.remove(it.next()));
        }
        assertTrue(expected.isEmpty());
    }

    @After
    public void tearDown()
    {
        System.setOut(sysOut);
        File file = new File("testGraph.txt");
        File empty = new File("empty.txt");

        if(file.delete() && empty.delete())
        {
            System.out.println(file.getName() + ", " + empty.getName() + ": File deleted successfully");
        }
        else
        {
            System.out.println(file.getName() + ", " + empty.getName() +
                    ": Failed to delete the file at " + file.getAbsolutePath() + "\n" + empty.getAbsolutePath() + "\n");
        }
    }
}

