package se465;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class PairTest {

    @Test
    public void testEqualToSelf()
    {
        Pair<String, String> a = new Pair<>("a", "b");
        assertEquals("Object not equal to itself", a, a);
    }

    @Test
    public void testNotEqualToNull()
    {
        Pair<String, String> a = new Pair<>("a", "b");
        assertNotEquals("Object comparison with null returned true", a, null);
    }

    @Test
    public void testNotEqualToOther()
    {
        Pair<String, String> a = new Pair<>("a", "b");
        assertNotEquals("Object comparison with other object returned true", a, new Object());
    }

    @Test
    public void testToString()
    {
        Pair<Integer, Integer> a = new Pair<>(1, 2);
        assertEquals(a.toString(), "(1, 2)");
    }

    @Test
    public void testEqual()
    {
        Pair<String, String> a = new Pair<>("abc", "def");
        Pair<String, String> another_a = new Pair<>("abc", "def");
        assertEquals(a, another_a);
    }

    @Test
    public void testNotEqual()
    {
        Pair<String, String> a = new Pair<>("abc", "def");
        Pair<String, String> b = new Pair<>("abc", "dep");
        assertNotEquals(a, b);
    }

    @Test
    public void testHashCode()
    {
        Pair<String, String> a = new Pair<>("abc", "def");
        assertEquals(a.hashCode(), ("abc".hashCode() * 10) ^ "def".hashCode());
    }
}

