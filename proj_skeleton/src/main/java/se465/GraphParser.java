package se465;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class GraphParser {
    public CallGraphNode parseLine(String line) {
        String[] splitted = line.split("'", 0);
        if (splitted.length < 2) {
            return null;
        } else {
            return new CallGraphNode(line.split("'", 0)[1]);
        }
    }

    public CallGraph parse(String filePath) {
        CallGraph result = new CallGraph();
        CallGraphNode curFrom = null;
        File f = new File(filePath);
        try (Scanner reader = new Scanner(f, "UTF-8")){
            while (reader.hasNextLine()) {
                String line = reader.nextLine();
                if (line.length() == 0) continue;
                if (line.contains("<<null function>>")) {
                    curFrom = null;
                }
                if (line.endsWith("'")) {
                    // case where the line is of format
                    // `  CS<0x3d03b50> calls function 'apr_table_setn'`
                    if (curFrom != null) {
                        CallGraphNode parsed = parseLine(line);
                        if (parsed != null) {
                            result.addEdge(curFrom, parsed);
                        }
                    }
                } else if (!line.endsWith("de")){
                    // case where the line is of format
                    // `Call graph node for function: 'ap_send_interim_response'<<0x3a0b740>>  #uses=2`
                    CallGraphNode parsed = parseLine(line);
                    if (parsed != null) {
                        curFrom = parseLine(line);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("** File Not Found Error");
            throw new RuntimeException(e.getMessage());
        }
        return result;
    }
}
