package se465;

import java.io.FileOutputStream;
import java.io.IOException;

import static java.util.Objects.requireNonNull;

public class Main {

  public static void main(String[] args) {
    int T_SUPPORT = 3;
    int T_CONFIDENCE = 65;
    String CALLGRAPH_FILE = null;
    String OUTPUT_FILE = null;

    if (args.length != 8) {
      printCommandHelper();
      throw new IllegalArgumentException("Invalid command line arguments");
    }
    // parsing cli args
    for (int i = 0; i < args.length; ++i) {
      switch (args[i]) {
        case "--support":
          T_SUPPORT = Integer.parseInt(args[++i]);
          break;
        case "--confidence":
          T_CONFIDENCE = Integer.parseInt(args[++i]);
          break;
        case "--callgraph":
          CALLGRAPH_FILE = args[++i];
          break;
        case "--output":
          OUTPUT_FILE = args[++i];
          break;
        default:
          printCommandHelper();
          throw new IllegalArgumentException("Command line arguments are invalid");
      }
    }
    // checks whether any command line args is still missing
    if (CALLGRAPH_FILE == null || OUTPUT_FILE == null) {
      printCommandHelper();
      throw new IllegalArgumentException("Required command line arguments missing");
    }

    // parse in the data from the input file
    GraphParser parser = new GraphParser();
    CallGraph callGraph = parser.parse(CALLGRAPH_FILE);

    // perform analytics on the data
    Analyzer analyzer = new Analyzer(T_SUPPORT, T_CONFIDENCE);
    analyzer.consume(callGraph);

    // store the data to the output file
    try (FileOutputStream fs = new FileOutputStream(requireNonNull(OUTPUT_FILE))) {
      analyzer.persistAnalytics(fs);
    } catch (IOException e) {
      // something's wrong with the output file path
      throw new RuntimeException(e);
    }
  }

  public static void printCommandHelper() {
    System.out.println("Please enter arguments as follows:");
    System.out.println("--support <T_SUPPORT>");
    System.out.println("--confidence <T_CONFIDENCE>");
    System.out.println("--callgraph <CALLGRAPH_FILE>");
    System.out.println("--output <OUTPUT_FILE>");
    System.out.println("Note: --callgraph and --output flags are mandatory");
  }
}
