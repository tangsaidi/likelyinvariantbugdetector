package se465;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.*;

public class Analyzer {

    private final int T_SUPPORT;
    private final int T_CONFIDENCE;
    // Stores all the pairs where a node is contained
    private Map<CallGraphNode, Set<Pair<CallGraphNode, CallGraphNode>>> nodeAndItsComrades = new HashMap<>();
    // Stores all the parents of a pair
    private Map<Pair<CallGraphNode, CallGraphNode>, Set<CallGraphNode>> pairAndItsSupport = new HashMap<>();
    private CallGraph graph;

    public Analyzer(int t_SUPPORT, int t_CONFIDENCE) {
        T_SUPPORT = t_SUPPORT;
        T_CONFIDENCE = t_CONFIDENCE;
    }

    /**
     * Analyze a {@link CallGraph} and store the result to the current Analyzer object.
     * @param callGraph CallGraph data object to be analyzed.
     */
    public void consume(CallGraph callGraph) {
        this.graph = callGraph;
        nodeAndItsComrades = new HashMap<>();
        pairAndItsSupport = new HashMap<>();
        Iterator<CallGraphNode> it = graph.iterator();
        while (it.hasNext()) {
            CallGraphNode visiting = it.next();
            nodeAndItsComrades.put(visiting, new HashSet<>());
        }

        it = graph.iterator();
        while (it.hasNext()) {
            CallGraphNode visiting = it.next();
            Set<Pair<CallGraphNode, CallGraphNode>> children = graph.getAllChildrenPairs(visiting);
            for (Pair<CallGraphNode, CallGraphNode> pair : children) {
                nodeAndItsComrades.get(pair.first).add(pair);
                nodeAndItsComrades.get(pair.second).add(pair);
                // Adds the parent to the parent set
                // This counts how many parents there are, which is the same as the number of supports
                Set<CallGraphNode> newParents = pairAndItsSupport.getOrDefault(pair, new HashSet<>());
                newParents.add(visiting);
                pairAndItsSupport.put(pair, newParents);
            }
        }
    }

    /**
     * Report current analytics to a string
     *   "bug: A in scope2, pair: (A, B), support: 3, confidence: 75.00%"
     */
    public String reportAnalytics() {
        Iterator<CallGraphNode> it = graph.iterator();
        StringBuilder sb = new StringBuilder();
        while (it.hasNext()) {
            CallGraphNode visiting = it.next();
            for (Pair<CallGraphNode, CallGraphNode> coms : nodeAndItsComrades.get(visiting)) {
                int comsSupport = pairAndItsSupport.get(coms).size();
                int nodeSupport = graph.getUsedCount(visiting);
                double confidence = comsSupport / (double) nodeSupport * 100;
                if (comsSupport >= T_SUPPORT && confidence >= T_CONFIDENCE) {
                    Set<CallGraphNode> pairParentsSet = pairAndItsSupport.get(coms);
                    Set<CallGraphNode> visitingParentSet = graph.getUsedBy(visiting);
                    Set<CallGraphNode> copy = new HashSet<>(visitingParentSet);
                    copy.removeAll(pairParentsSet);
                    for (CallGraphNode parent : copy) {
                        sb.append(String.format("bug: %s in %s, pair: %s, support: %d, confidence: %.2f%%%n",
                                visiting.getId(),
                                parent.getId(),
                                coms.toString(),
                                comsSupport,
                                confidence ));
                    }
                }
            }
        }
        return sb.toString();
    }


    /**
     * Store the analyzed result to an {@link OutputStream}.
     * Analyzer is not responsible for closing the OutputStream object.
     * @param os Target OutputStream for the analytics data to be stored.
     */
    public void persistAnalytics (OutputStream os) throws IOException {
        byte[] data = reportAnalytics().getBytes(Charset.forName("UTF-8"));
        os.write(data, 0, data.length);
    }
}
