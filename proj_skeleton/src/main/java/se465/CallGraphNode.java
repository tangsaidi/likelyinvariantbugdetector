package se465;

public final class CallGraphNode implements Comparable<CallGraphNode> {
    private final String id;

    CallGraphNode(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return id;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof CallGraphNode)){
            return false;
        }
        CallGraphNode other = (CallGraphNode) obj;
        return id.equals(other.id);
    }

    @Override
    public int compareTo(CallGraphNode other) {
        return this.id.compareTo(other.id);
    }
}