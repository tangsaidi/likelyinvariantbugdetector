package se465;

import java.util.*;

/**
 * Data object for doing analytics.
 * The CallGraph is a directed graph implemented in an adjacency list
 * Edges in the CallGraph are directed from higher level nodes to lower level nodes
 * This way we can easily generate every pair of children from the adjacency list
 */
public class CallGraph implements Iterable {
    private Map<CallGraphNode, Set<CallGraphNode>> graph;
    private Map<CallGraphNode, Set<CallGraphNode>> usedBy;

    public CallGraph() {
        graph = new HashMap<>();
        usedBy = new HashMap<>();
    }

    private boolean lessThan(int a, int b) {
        return a < b;
    }

    // Adds a node into the graph if it does not yet exist
    public void addNode(CallGraphNode node) {
        if (!graph.containsKey(node)) {
            graph.put(node,new HashSet<>());
        }
        if (!usedBy.containsKey(node)) {
            usedBy.put(node, new HashSet<>());
        }
    }

    // Add an edge into the adjacency list
    // If the edge size grew by one, we increment the used count
    public void addEdge(CallGraphNode from, CallGraphNode to) {
        addNode(from);
        addNode(to);
        graph.get(from).add(to);
        usedBy.get(to).add(from);

    }

    public int getUsedCount(CallGraphNode node) {
        return usedBy.get(node).size();
    }

    public Set<CallGraphNode> getUsedBy(CallGraphNode node) {
        return usedBy.get(node);
    }

    public Set<Pair<CallGraphNode, CallGraphNode>> getAllChildrenPairs(CallGraphNode node) {
        // This part is not quite memory efficient. There might be a way to improve it.
        CallGraphNode[] children = graph.get(node).toArray(new CallGraphNode[0]);
        Set<Pair<CallGraphNode, CallGraphNode>> childrenPairs = new HashSet<>();
        for (int i = 0; lessThan(i, children.length); ++i) {
            for (int j = i + 1; lessThan(j, children.length); ++j) {
                Pair<CallGraphNode, CallGraphNode> toAdd;
                // This makes sure the pairs returned are always ordered in lexi order
                if (lessThan(children[i].compareTo(children[j]), 0)) {
                    toAdd = new Pair<>(children[i], children[j]);
                } else {
                    toAdd = new Pair<>(children[j], children[i]);
                }
                childrenPairs.add(toAdd);
            }
        }
        return childrenPairs;
    }

    public Iterator<CallGraphNode> iterator() {
        return new GraphIterator(this);
    }

    static class GraphIterator implements Iterator<CallGraphNode> {
        Iterator<CallGraphNode> it;
        Set<CallGraphNode> keySet;
        // constructor
        GraphIterator(CallGraph callGraph) {
            keySet = callGraph.graph.keySet();
            it = keySet.iterator();
        }

        // Checks if the next element exists
        public boolean hasNext() {
            return it.hasNext();
        }

        // moves the cursor/iterator to next element
        public CallGraphNode next() {
            return it.next();
        }

        // Used to remove an element. Implement only if needed
        public void remove() {
            throw new UnsupportedOperationException("Remove not implemented");
        }
    }
}
