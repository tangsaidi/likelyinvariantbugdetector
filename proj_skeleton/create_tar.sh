#!/usr/bin/env bash

set -o pipefail
set -o errexit
set -o nounset
set -o xtrace

if [[ "$#" != 1 ]] ; then
  echo "Usage: $0 <group number>"
  exit 1
fi	

readonly GROUP=$1

tar zcf "Group${GROUP}.tar.gz" pom.xml report/proj_sub.pdf src
